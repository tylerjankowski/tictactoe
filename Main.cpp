#include <iostream>
#include <conio.h>

using namespace std;

class TicTacToe {

private:
	char m_board[9];
	int m_numTurns;
	char m_playerTurn;
	char m_winner;

public:
	TicTacToe() {
		SetTurns(9);
		SetPlayerTurn('X');
		SetWinner(' ');
	}

	void SetTurns(int turns) { m_numTurns = turns; }
	void SetPlayerTurn(char player) { m_playerTurn = player; }
	void SetWinner(char winner) { m_winner = winner; }

	int GetTurns() { return m_numTurns; }
	char GetPlayerTurn() { return m_playerTurn; }
	char GetWinner() { return m_winner; }

	void DisplayBoard() {
		cout << m_board[0] << " " << m_board[1] << " " << m_board[2] << "\n";
		cout << m_board[3] << " " << m_board[4] << " " << m_board[5] << "\n";
		cout << m_board[6] << " " << m_board[7] << " " << m_board[8] << "\n";
	}

	bool IsOver() {
		// Check for X's
		// horizontal
		if (m_board[0] == 'X' && m_board[1] == 'X' && m_board[2] == 'X') {
			m_winner = 'X';
		}
		if (m_board[3] == 'X' && m_board[4] == 'X' && m_board[5] == 'X') {
			m_winner = 'X';
		}
		if (m_board[6] == 'X' && m_board[7] == 'X' && m_board[8] == 'X') {
			m_winner = 'X';
		}
		// vertical
		if (m_board[0] == 'X' && m_board[3] == 'X' && m_board[6] == 'X') {
			m_winner = 'X';
		}
		if (m_board[1] == 'X' && m_board[4] == 'X' && m_board[7] == 'X') {
			m_winner = 'X';
		}
		if (m_board[2] == 'X' && m_board[5] == 'X' && m_board[8] == 'X') {
			m_winner = 'X';
		}
		// diagonal
		if (m_board[0] == 'X' && m_board[4] == 'X' && m_board[8] == 'X') {
			m_winner = 'X';
		}
		if (m_board[2] == 'X' && m_board[4] == 'X' && m_board[6] == 'X') {
			m_winner = 'X';
		}


		// Check for O's
		// horizontal
		if (m_board[0] == 'O' && m_board[1] == 'O' && m_board[2] == 'O') {
			m_winner = 'O';
		}
		if (m_board[3] == 'O' && m_board[4] == 'O' && m_board[5] == 'O') {
			m_winner = 'O';
		}
		if (m_board[6] == 'O' && m_board[7] == 'O' && m_board[8] == 'O') {
			m_winner = 'O';
		}
		// vertical
		if (m_board[0] == 'O' && m_board[3] == 'O' && m_board[6] == 'O') {
			m_winner = 'O';
		}
		if (m_board[1] == 'O' && m_board[4] == 'O' && m_board[7] == 'O') {
			m_winner = 'O';
		}
		if (m_board[2] == 'O' && m_board[5] == 'O' && m_board[8] == 'O') {
			m_winner = 'O';
		}
		// diagonal
		if (m_board[0] == 'O' && m_board[4] == 'O' && m_board[8] == 'O') {
			m_winner = 'O';
		}
		if (m_board[2] == 'O' && m_board[4] == 'O' && m_board[6] == 'O') {
			m_winner = 'O';
		}

		if (m_winner == ' ') {
			return false;
		}
		return true;
	}

	bool IsValidMove(int i) {
		if (i >= 1 && i <= 9) {
			return true;
		}
		return false;
	}

	void Move(int m) {
		
		m_board[m-1] = m_playerTurn;
		if (m_playerTurn == 'X') {
			m_playerTurn = 'O';
		}
		else {
			m_playerTurn = 'X';
		}
	}

	void DisplayResult() {
		cout << m_winner << " has won! ";
	}

};

int main()
{
	TicTacToe* pGame = nullptr;

	while (true)
	{
		if (pGame) delete pGame;
		pGame = new TicTacToe;

		// play the game
		while (!pGame->IsOver())
		{
			pGame->DisplayBoard();

			int position;
			do
			{
				cout << "Player " << pGame->GetPlayerTurn() << ", select a position (1-9): ";
				cin >> position;
			} while (!pGame->IsValidMove(position));

			pGame->Move(position);
		}

		// game over
		pGame->DisplayBoard();
		pGame->DisplayResult();


		// prompt to play again (or quit)
		char input = ' ';
		while (input != 'Y' && input != 'y')
		{
			std::cout << "Would you like to play again? (y/n): ";
			cin >> input;

			if (input == 'N' || input == 'n') return 0; // quit
		}
	}
}
